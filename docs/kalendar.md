---
sidebar_position: 2
---

# Kalendář

Kalendář akcí, událostí a termínů pro aktuální akademický rok 2024/2025.

<table>
    <thead>
        <tr>
            <th>Datum</th>
            <th>Událost</th>
            <th>Poznámka</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1.10.2024</td>
            <td>Začátek výuky zimního semestru</td>
            <td></td>
        </tr>
        <tr>
            <td>7.10.2024 - 9.10.2024</td>
            <td>Soustředění katedry - Poněšice</td>
            <td></td>
        </tr>
        <tr>
            <td>6.10.2025 - 8.10.2025</td>
            <td>Soustředění katedry - Poněšice</td>
            <td></td>
        </tr>
    </tbody>
</table>
