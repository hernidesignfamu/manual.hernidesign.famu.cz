---
sidebar_position: 1
---

# Úvod do herního designu

## Role herního designéra v praxi

Obecná role herního designéra je ve větších týmech často dělena na konkrétní specializace. Naopak v menších týmech zahrnuje i další role a zodpovědnosti, které s designem souvisí přímo i nepřímo. Při vývoji her se často opakují některé problémy – v tvůrčí, manažerské i finanční sféře. Některé z nich lze včas identifikovat a tím jim do určité míry předejít nebo zmírnit jejich dopady.

### Způsob zapojení herního designéra a jeho činnost

- Vedoucí designér
    - Řídí designéry – rozděluje zodpovědnost, řídí komunikaci
    - Zaručuje ucelenost designu – udržuje jednotnou vizi a konzistenci designu
    - Zajišťuje spolupráci s dalšími odděleními – koordinace vize s vedoucím grafikem, zvukařem, animátorem, programátorem atd.
- Designer s vymezenou působností
    - Quest / level designér – navrhuje obsah konkrétních questů nebo levelů
    - Spisovatel / scénárista – píše textový obsah (dialogy, scénář)
    - Designér herních mechanik - např. soubojového systému, simulace světa, AI

### Design dokumenty – výstupy designera

Jedním z nutných výstupů práce designéra je i dokumentace popisující určité aspekty hry. Obecně neexistuje jedna konkrétní ustálená forma, protože téměř vždy závisí na typu projektu.  Přesto lze vysledovat rámcové rozdělení této dokumentace.

- Design dokument (Game Design Document)
    - Rámcově popisuje téma, vzhled, funkčnost, rozsah hry atd.
    - Příklad [zde](https://docs.google.com/document/d/1zrrDKwPaufB7QrtTkiq05OzhjvJgg_I0rC0MKRuX5Ao/edit).
    - Detailní popis často rozveden v separátních dokumentech.
- Design mechanik (např. stealth, inventář...)
    - pro programátory – technický popis, řeší i mezní případy (např. plný inventář)
    - designér musí rámcově znát možnosti a technická omezení herního enginu
- Scénář, dialogy
    - Text, často obsahuje i jednoduché herní příkazy (např. zatmívačka, přidání předmětu do inventáře, přehrání zvuku), např:
        - Blacksmith: Here’s your sword! {addItem(“sword”,1)}
- Koncepty (postav, lokací atd.) pro výtvarníky
    - Reference – obrázky, videa, texty (často i z jiných her)

### Zapojení designera v menších týmech – související role

Především v menších týmech s omezenými lidskými zdroji je to právě často designér, kdo supluje další potřebné role.

- Tester – hledá a zaznamenává nalezené chyby (a zjišťuje, jak je zreprodukovat)
- Project manager – přidělování úkolů, plánování dalšího vývoje
- Community manager – komunikace s hráči
- Příprava lokalizace – zadávání a kontrola překladů
- Casting dabérů, režie dabingu
- Playtesty – příprava, vedení, vyhodnocování
- Prezentace – veletrhy, PR, příprava materiálů
- Herní nápověda – tutoriály, manuál
- Audio designer – shánění a střih zvuků

### Problémy při vývoji her z pohledu herního designu a produkce

#### Tvůrčí

- absence prototypování
    - Hratelný prototyp může a měl by vzniknout v řádech jednotek dní za účelem ověření funkčnosti designu. Pokud to tak není, tým může ztrácet čas prací na nefunkčním konceptu.
- nedostatek iterace a playtesty 
    - Bez playtestů se může příliš pozdě zjistit, že jádro hry není pro hráče dostatečně jasné nebo zajímavé.
- nejasná vize, chybí tah na bránu
    - Není jasné, “o čem” hra je a jaký zážitek si z ní má hráč odnést.
    - Jednotlivé prvky působí nesourodým dojmem.
- zadavatel (investor, publisher) změní zadání
- nejasná cílová skupina
    - Není například vhodné tvořit komplexní logickou hru pro tříleté děti.
- cílová platforma nebo žánr vyjde z módy
    - Android konzole – OUYA, Gamestick

#### Manažerské

- špatná týmová komunikace
    - Část týmu rezignuje na tvůrčí přístup a komunikaci a pouze mechanicky plní zadání.
- nedostatek zkušených lidí 
    - Pro tým může být citelný zejména nedostatek programátorů nebo výtvarníků s technickým přístupem, kteří mohou odevzdat grafiku v požadovaném formátu.
- špatný odhad časové náročnosti
    - Během produkce – tým zvolí příliš velký rozsah nebo příliš detailní styl.
    - Během postprodukce – nedostatečně se testují finální buildy pro jednotlivé platformy (například: na PC to běželo dobře, na mobilu se to trhá).
- selhalo plánování
    - Výsledek špatného plánování je crunch – přesčasy, v některých případech i po do dobu několika měsíců. Ten má zpětně negativní dopad na motivaci a produktivitu týmu.

#### Finanční

- zbytečně velký tým
    - V určitých fázích vývoje mohou být některé týmové role nevytížené.
- akcelerátor nebo soutěž 
    - Problém může nastat, když slíbená výhra nebo pobídka není dodána.
- omezené grantové možnosti
    - V ČR dosud není mnoho možností získat grant na vývoj her.

## Rizika a příležitosti studentských her

Cílem studentského projektu je především ho dokončit. I mnoho her tvořených týmem profesionálů končí nezdarem a v případě studentů s malou nebo žádnou předchozí zkušeností je to výzva o to větší. Po zdárném dokončení může následovat reflexe, poučení se z chyb a zlepšení do příštích projektů. I průměrná dokončená hra bude pro získání zkušeností mnohem lepší než úžasný koncept, který si ale nikdo nikdy nemohl ani vyzkoušet.

V textu používám často termín tým, ale aplikovatelné je to téměř vždy i na solitérní autory. Bojovat sám se sebou je koneckonců často skoro až týmová práce.

### Rozsah práce (scope)

Vhodný rozsah projektu je pro dokončení vůbec to nejzásadnější. S problémy s rozsahem se setkáváme u naprosté většiny studentských projektů. Snahou týmu by mělo být vymyslet si takový projekt, který bude náročností odpovídat schopnostem tvůrců a bude v daném čase kompletně dokončitelný. A nutno dodat, že by neměl být ani příliš jednoduchý, aby se u něj studenti nenudili. Ostatně stavu flow (kdy provádíme činnost, která ve správné míře překračuje naše schopnosti) budeme chtít dosáhnout i u hráčů, tak proč nezačít nejprve u sebe.

### Velké ambice

Hodně začínajících tvůrců si naloží napoprvé moc velké sousto. Nejkřiklavějším příkladem je MMORPG, designově i technologicky jeden z nejsložitějších žánrů.

I přestože si vybereme jednodušší žánr, stále je možné snadno přepálit množství mechanik, featur a obsahu. Začátečníkům nelze mít za zlé, že realizovatelnost nezvládnou správně odhadnout. I v profesionálním vývoji se totiž běžně setkáváme s odklady vydání. Jak se tedy pokusit správně odhadnout rozsah?

Nechte si poradit. Najděte ve svém okolí někoho, komu věříte a kdo dokáže co nejlépe odhadnout vaše schopnosti. Představte mu svůj projekt a buďte otevření změnám.

Rozdělte si projekt na menší milníky. Z celého projektu můžete vybrat jednu základní jednoduchou mechaniku a tu nejprve realizovat. Pokud vám například pohyb postavy zabral dva měsíce práce, je jasné, že detektivní adventuru s přetáčením času a větvenými dialogy bude velký problém dokončit za půl roku.

Nebojte se, nic vám neuteče. Svou velkolepou myšlenku můžete určitě zkusit zrealizovat v některé z příštích her. Pokud do té doby nasbíráte dost zkušeností, bude navíc pravděpodobně výsledek o dost lepší, než pokud svou vysněnou myšlenku realizujete jako první projekt bez zkušeností. Dá se na to však jít i obráceně. Máte skvělou příběhovou zápletku nebo konkrétní mechaniku? Nestavte kolem ní příliš velkou hru, naopak se pokuste destilovat daný zážitek do co nejjednoduššího produktu, který přesto bude hráčsky srozumitelný a dokončitelný.

### Hra je od začátku plánovaná pouze jako část většího celku

Další věc se kterou se občas setkáváme je, že studenti naplánují velkolepou ambiciózní hru, nicméně se řídí předchozími doporučeními, uvědomují si nerealizovatelnost a pokouší se rozsah projektu redukovat. Namísto redukce samotného projektu však sahají pouze k redukci cíle, který si vytyčí v rámci školního úkolu. Z 10 hodinové příběhové adventury tak vzniká hodinová verze, která se má zvládnout dokončit v daném čase s tím, že půjde právě jen o výsek větší hry a případným příslibem že se na projektu bude pokračovat i po ukončení školních povinností.

Potenciální problém zde nastává kvůli tomu, že motivace týmu po splnění povinností klesá a ke hře se s velkou pravděpodobností (čest výjimkám) tvůrci už nikdy nevrací. Ta tak zůstává na věky nedokončená. I energie se tříští, protože se autoři musí zabývat nejen tím, co aktuálně dělají, ale neustále myslet i na to, jak to bude fungovat s navazujícími částmi, které přitom nejspíš ani nikdy nevytvoří.

Ještě větším úskalím je získání zpětné vazby. Při prezentaci nebo playtestech takového projektu budete nutně postupovat tak, že se snažíte publikum přesvědčit ne pouze o kvalitách toho, co jste udělali, ale i toho co jste neudělali (a nejspíš nikdy neuděláte). Stále se bude vnucovat myšlenka, že to je jen desetina hry, ale pak tam budou přeci i “ty další věci” a celkově to tak bude o dost lepší. Takový přístup může fungovat při přesvědčování případných partnerů, ale nebude fungovat pro uzavření tvůrčího cyklu a získání zkušeností. Mnohem více se naučíte na projektu, který je plně dokončený a dá se u něj reflektovat, zda splnil očekávání a přinesl kýžený hráčský zážitek. U části hry se toto dělá o srovnání hůře; příběh navíc není možné uzavřít, pokud ho neodvyprávíme celý.

### Tým

Na hrách se ve většině případů pracuje v týmech. Správné rozdělení a organizace práce, komunikace a motivace jsou tak dalším klíčovým faktorem úspěchu. Ve vývojářském týmu fungují vztahy jako kdekoli jinde. Pokud si s někým nesednete, někdo nekomunikuje, není motivovaný, brzdí vás, je asi vhodné se zamyslet, zda s takovým člověkem pracovat. Na druhou stranu ale mějme stále na paměti to nejdůležitější – dokončit projekt. Může tak být lepší zatnout zuby a projekt se snažit dokončit, překážky brát jako výzvy a snažit se ostatní členy týmu podpořit. Po dokončení je ale čas reflektovat, jak tým fungoval a zda bude plodná i budoucí spolupráce.

Komunikujte. Je na vás jakým způsobem a jak často, ale pokud má tým fungovat, musí být všichni v obraze, alespoň co se týče své vlastní práce. Častá komunikace prospívá i vzájemné motivaci. Hodně studentských týmů se rozpadá právě proto, že někteří z členů přestanou komunikovat. Místo řešení a vyjasnění problému radši strčí hlavu do písku a už od nich nikdy neuslyšíte. Další možnost je, že u nich opadne nadšení a motivace k projektu. Výhodou profesionálních týmů je, že mají jednu silnou externí motivaci a tím jsou peníze. Pokud je to motivace jediná, není to ideální, ale pomůže překlenout některá období. U studentských projektů s finanční odměnou nepočítáme, a tak je potřeba co nejvíce využívat motivaci vnitřní.

Můžete si zkusit vypůjčit něco z metod agilního vývoje jako např. denní standupy, týdenní demo nebo plánování sprintů. Můžete si udělat víkendový game jam nebo si třeba vyhradit jeden den v týdnu, kdy se všichni na několik hodin potkáte. Zajistěte, ať každý člen týmu má přístup k aktuální verzi hry a ať má možnost vidět svou práci a práci ostatních členů ve hře. Když grafik kreslí do šuplíku a výsledek nevidí zapojený do hry, jeho motivace klesá. Programátor bude také radši pracovat s podklady od skvělého týmového grafika než s ošklivými placeholdery.

### Další zdroje motivace

Motivace je důležitá věc. Peníze nemáme, v týmu se navzájem jakž takž motivujeme, ale chtělo by to něco víc. Motivaci můžeme čerpat i vně týmu.

#### Playtesty a zpětná vazba

Nebojte se dávat hru zahrát lidem. Není dobré v tajnosti kutit váš geniální nápad a představovat si, jak se u toho hráči budou skvěle bavit, abyste následně zjistili, že se nedostanou ani z prvního levelu, protože nepochopili ovládání. A přitom by třeba stačila jen malá změna. Čím častěji budete dělat playtesty, tím více se dozvíte, co ve hře funguje a co nefunguje. Pokud budete svou práci dělat dobře, uvidíte postupem času více nadšených výrazů a ujištění, že postupujete správným směrem, což vám dodá další energii. Nechte si dávat zpětnou vazbu. Ale pozor – pokud má hráč pocit, že je něco špatně, má rozhodně pravdu a je potřeba s tím něco dělat. Často se však hráč mýlí v návrzích řešení. Vy znáte svou hru lépe a přijít s řešením je vaše práce.

#### Veřejné prezentace

Pokud můžete, prezentujte veřejně. Opět vám to dodá motivaci, naučíte se vystupovat a hru lépe prodat. Kvalita některých prezentací studentských projektů v Česku je špatná a bohužel ani v profesionální sféře to nemusí být o tolik lepší. V tomto ohledu za západními zeměmi citelně ztrácíme. Ale ani to by nás právě nemělo odrazovat, naopak. Prezentaci nebo pitch se naučíme nejlépe mluvením na veřejnosti. Většina konferencí a festivalů dává prostor i studentské tvorbě. Využijte ho a ukažte svou hru veřejnosti a profesionálům z oboru.

#### Akce a soutěže

Soutěží se u nás poslední dobou příliš nekoná a je to docela škoda. Je to skvělá příležitost, jak se vybičovat k dokončení hry a získat třeba i nějakou zpětnou vazbu od odborné poroty. V poslední době ale získávají hodně na oblibě game jamy, tedy víkendové maratony v tvorbě her. Rozdělanou hru však na game jam neberte. Raději přijďte s čistou hlavou a zkuste si z víkendu odnést zkušenosti, osahat si nějaké nové postupy, utužit tým nebo zkusit najít tým nový. Za takový víkend můžete snadno nasbírat zkušenosti, které byste jinak získávali dlouhé týdny a měsíce.

### Závěr – studentská hra jako nekomerční projekt

Na závěr je dobré si uvědomit, že při tvorbě studentské hry se nemusíte snažit o komerční hit. Stejně nebude. Komerční úspěch se po vás bude chtít pravděpodobně po zbytek života, tak proč se tím trápit už ve studentských letech. Peníze navíc nesou spoustu rizik – licenční, právní, vztahová atd. Komerční potenciál má smysl začít řešit až ve chvíli, kdy budete mít tak kvalitní hru, že se nebudete stydět si za ní říct o peníze od hráčů. Na druhou stranu to neznamená, že byste měli zbytečně snižovat své ambice. Udělejte hru, která bude skvělá a úspěšná. Měřítka úspěchu si můžete nastavit sami a finance do toho prozatím nepočítat. Experimentujte a udržujte si hlavu otevřenou, dokud máte tu možnost, zároveň se ale nezapomeňte naučit i kus užitečného řemesla.

K dalšímu vhledu do základů herního designu doporučujeme knihy [Tracy Fullerton - Workshop herního designu](https://namu.cz/workshop-herniho-designu) a další knihy v seznamu doporučených studijních materiálů.