---
sidebar_position: 3
---

# Rozvrhy

Níže naleznete rozvrhy pro zimní a letní semestr. Pokud je rozvrh potvrzený, znamená to, že se takto předměty vypíšou. Předběžný rozvrh není závazný a ještě se může změnit.

## LS - Potvrzený pro LS 2025

<iframe src="https://docs.google.com/spreadsheets/d/1iiR6dDhRdT-711wgmtFaT5KUPX9HYTWq/pubhtml?gid=1792591129&amp;single=true&amp;widget=true&amp;headers=false" class="iframe"></iframe>

[Otevřít rozvrh v novém okně](https://docs.google.com/spreadsheets/d/1iiR6dDhRdT-711wgmtFaT5KUPX9HYTWq/edit?usp=sharing&ouid=110076813591903663396&rtpof=true&sd=true)

## ZS - Předběžný pro ZS 2025

<iframe src="https://docs.google.com/spreadsheets/d/1t12viCmaJKZ31cvwN1t_Y44iFRYLVcnA/pubhtml?gid=834453386&amp;single=true&amp;widget=true&amp;headers=false" class="iframe"></iframe>

[Otevřít rozvrh v novém okně](https://docs.google.com/spreadsheets/d/1t12viCmaJKZ31cvwN1t_Y44iFRYLVcnA/edit?usp=sharing&ouid=110076813591903663396&rtpof=true&sd=true)
