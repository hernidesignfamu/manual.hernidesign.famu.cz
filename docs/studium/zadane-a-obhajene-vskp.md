# Zadané a obhájené VŠKP

Seznam teoretických diplomových prací, které byly na katedře v minulosti obhájeny nebo jsou momentálně zadány.

<table>
    <tr>
        <th>Název práce</th>
        <th>Student/ka</th>
        <th>Školitel/ka</th>
    </tr>
    <tr>
        <td class="s1">Pod hladinou zábavy</td>
        <td class="s1">Josef Šmíd</td>
        <td class="s1" dir="ltr">BENDOVÁ Helena</td>
    </tr>
    <tr>
        <td class="s1">Transgresivní estetika ve hrách NEWGROUNDS.COM mezi lety 2000 - 2005</td>
        <td class="s1">Mikoláš Fišer</td>
        <td class="s1" dir="ltr">ŠVELCH Jaroslav</td>
    </tr>
    <tr>
        <td class="s1">Vývoj terapeutických her v ČR</td>
        <td class="s1">Jitka Uhříčková</td>
        <td class="s1" dir="ltr">FOUSEK KROBOVÁ Tereza </td>
    </tr>
    <tr>
        <td class="s1">Metafora ekonomického růstu ve videoherním designu</td>
        <td class="s1">Matěj Hložánek</td>
        <td class="s1" dir="ltr">BENDOVÁ Helena </td>
    </tr>
    <tr>
        <td class="s1">Historický vývoj inventářů v RPG hrách</td>
        <td class="s1">Tereza Kotěšovcová</td>
        <td class="s1" dir="ltr">Švelch Jaroslav</td>
    </tr>
    <tr>
        <td class="s1">Reprezentace drog v počítačových hrách</td>
        <td class="s1">Petra Emmerová</td>
        <td class="s1" dir="ltr">FOUSEK KROBOVÁ Tereza</td>
    </tr>
    <tr>
        <td class="s1">Obsazeno! O funkci, významu a podobě toalet v počítačových hrách.</td>
        <td class="s1">Kateřina Hanáčková</td>
        <td class="s1" dir="ltr">Švelch Jaroslav</td>
    </tr>
    <tr>
        <td class="s1">Kosmický horor z pohledu herního designu</td>
        <td class="s1">Vendula Burešová</td>
        <td class="s1" dir="ltr">FOUSEK KROBOVÁ Tereza</td>
    </tr>
    <tr>
        <td class="s1">Fenomén deformovaného tělesna NPC v kontextu videoherního hororu</td>
        <td class="s1">Lucie Formánková</td>
        <td class="s1" dir="ltr">ANGER Jiří</td>
    </tr>
    <tr>
        <td class="s1">Formální přístupy k prezentování videoher v galerijních prostorech</td>
        <td class="s1">Jan Froněk</td>
        <td class="s1" dir="ltr">SÝKORA Andrej</td>
    </tr>
    <tr>
        <td class="s1">Explorace toxického chování: Jeho vliv na výkon hráčů hry Counter-Strike 2</td>
        <td class="s1">Ondřej Dolejš</td>
        <td class="s1" dir="ltr">FOUSEK KROBOVÁ Tereza</td>
    </tr>
    <tr>
        <td class="s1">Využívání náhody v procedurálních systémech k oživení herních světů.</td>
        <td class="s1">Vít Paulík</td>
        <td class="s1" dir="ltr">SÝKORA Andrej </td>
    </tr>
    <tr>
        <td class="s1">Narativy a prostředí survival budovatelských strategií</td>
        <td class="s1">Martin Hromádka</td>
        <td class="s1" dir="ltr">FOUSEK KROBOVÁ Tereza</td>
    </tr>
    <tr>
        <td class="s1">Antropomorfní zvířata jako protagonisté video her</td>
        <td class="s1">Kryštof Ježek</td>
        <td class="s1" dir="ltr">BENDOVÁ Helena</td>
    </tr>
</table>