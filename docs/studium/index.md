---
sidebar_position: 2
---

# Studium

## Kredity

Během studia musíte získat 120 kreditů. Část kreditů získáte absolvováním povinných předmětů, část absolvováním povinně-volitelných předmětů a část absolvováním volitelných předmětů.

### Povinné předměty

- z povinných předmětů behem studia získáte 94 kreditů
- povinné předměty se vám každý semestr zapíší automaticky
- povinné předměty je vždy nutné úspěšně absolvovat (výjimkou je rozložení nebo přerušení studia a předmět Anglický jazyk, který lze absolvovat v libovolném ročníku studia)

### Povinně volitelné předměty

- z povinně volitelných předmětů během studia získáte 18 kreditů rozdělených do 3 skupin
    - Multimediální tvorba - 8 kreditů
    - Společenské a kulturní kontexty her - 6 kreditů
    - Dějiny a teorie umění a společenské vědy - 4 kredity
- optimální je tak v každém semestru studia získat alespoň 4-5 kreditů z PV předmětů

### Volitelné předměty

- všechny ostatní předměty, které si zapíšete nad rámec výše uvedených kategorií, se započítají jako volitelné
- z volitelných předmětů během studia musíte získat minimálně 8 kreditů
- optimální je tak v každém semestru studia získat alespoň 2 kredity z volitelných předmětů

## Diplomová práce

Téma a školitele diplomové práce je potřeba zadat do KOSu nejpozději 20. února v druhém roce studia. Kompletní informace o zadávání VŠKP naleznete v metodickém pokynu [zde](/files/studium/vskp.pdf).

Práci je nutné odevzdat nejpozději 1 měsíc před konáním státnic.

Školitelé:

<table>
    <tr>
        <th>Jméno a příjmění</th>
    </tr>
    <tr>
        <td>Helena Bendová</td>
    </tr>
    <tr>
        <td>Tereza Fousek-Krobová</td>
    </tr>   
    <tr>
        <td>Tomáš Oramus</td>
    </tr>     
    <tr>
        <td>Andrej Sýkora</td>
    </tr>
    <tr>
        <td>Jaroslav Švelch</td>
    </tr>
</table>

[Seznam zadaných a obhájených VŠKP](zadane-a-obhajene-vskp)

## Absolventský výkon

Název absolventského výkonu (hry) a vedoucího práce (zpravidla vedoucí dílny 2. ročníku) je potřeba zadat do KOSu nejpozději 20. února v druhém roce studia. Kompletní informace o zadávání VŠKP naleznete v metodickém pokynu [zde](/files/studium/vskp.pdf).

## Státnice

- 25.9.2023 (Pondělí) - obhajoby teoretických diplomových prác a státnice na katedře
- 26.9.2023 (Úterý) - obhajoby praktických diplomových prác na katedře

### Státnicové otázky

- okruh [Herní design](https://docs.google.com/document/d/1qcPT9RAZHcpBFQm0MfICWmrIrTpiNZX_14savAkgv7k/edit?usp=sharing)
- okruh [Herní historie a teorie](https://docs.google.com/document/d/1GXpOUOr71F1dcm3dXypmProLntVGD2QYV_IkQ9K0vjc/edit?usp=sharing)
- okruh [Multimediální tvorba](https://docs.google.com/document/d/1SLOREmdAwTvbQfi5phNZ6dYO_JuSyDbsP-SbMNZM1Kw/edit?usp=sharing)
