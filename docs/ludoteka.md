---
sidebar_position: 5
---

# Ludotéka

KHD disponuje 

## Chci se hrát



## Seznam her

### PC - Steam

<table><tr><th>Název hry</th><th>Dekáda</th><th>Rok vydání</th><th>Autor / Studio</th><th>Platforma</th></tr>
<tr>
  <td>Indiana Jones and the Last Crusade</td>
  <td>1980</td>
  <td>1989</td>
  <td>LucasArts</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Zork Anthology</td>
  <td>1980</td>
  <td>1989</td>
  <td>Infocom</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Loom</td>
  <td>1990</td>
  <td>1990</td>
  <td>Lucasfilm</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Another World</td>
  <td>1990</td>
  <td>1991</td>
  <td>Eric Chahi</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Indiana Jones and the Fate of Atlantis</td>
  <td>1990</td>
  <td>1992</td>
  <td>LucasArts</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Wolfenstein 3D</td>
  <td>1990</td>
  <td>1992</td>
  <td>ID Software</td>
  <td>Steam</td>
</tr>
<tr>
  <td>System Shock: Enhanced Edition</td>
  <td>1990</td>
  <td>1994</td>
  <td>Looking Glass Studios</td>
  <td>Steam</td>
</tr>
<tr>
  <td>I Have no mouth and I must scream</td>
  <td>1990</td>
  <td>1995</td>
  <td>The Dreamers</td>
  <td>Steam</td>
</tr>
<tr>
  <td>The Dig</td>
  <td>1990</td>
  <td>1995</td>
  <td>Lucasfilm</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Master of Orion 1</td>
  <td>1990</td>
  <td>1996</td>
  <td>Simtex</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Master of Orion 2</td>
  <td>1990</td>
  <td>1996</td>
  <td>Simtex</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Oddworld: Abe&#39;s Odyssey</td>
  <td>1990</td>
  <td>1997</td>
  <td>Oddworld Inhabitants</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Fallout 2</td>
  <td>1990</td>
  <td>1998</td>
  <td>Interplay</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Half-life</td>
  <td>1990</td>
  <td>1998</td>
  <td>Valve</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Oddworld: Abe&#39;s Exodus</td>
  <td>1990</td>
  <td>1998</td>
  <td>Oddworld Inhabitants</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Heroes of Might &amp; Magic III - HD Edition</td>
  <td>1990</td>
  <td>1999</td>
  <td>3DO</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Myst</td>
  <td>1990</td>
  <td>1999</td>
  <td>Cyan</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Oucast 1.1</td>
  <td>1990</td>
  <td>1999</td>
  <td>Fresh3d</td>
  <td>Steam</td>
</tr>
<tr>
  <td>System Shock 2</td>
  <td>1990</td>
  <td>1999</td>
  <td>Irrational Games</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Deus Ex</td>
  <td>2000</td>
  <td>2000</td>
  <td>Ion Storm</td>
  <td>Steam</td>
</tr>
<tr>
  <td>The Longest Journey</td>
  <td>2000</td>
  <td>2000</td>
  <td>Funcom</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Wizardry 8</td>
  <td>2000</td>
  <td>2001</td>
  <td>Sirtech</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Elder Scrolls Morrowind III</td>
  <td>2000</td>
  <td>2002</td>
  <td>Bethesda</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Mafia</td>
  <td>2000</td>
  <td>2002</td>
  <td>Illusion Softworks</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Syberia</td>
  <td>2000</td>
  <td>2002</td>
  <td>Microids</td>
  <td>Steam</td>
</tr>
<tr>
  <td>The Elder Scrolls: Morrowind</td>
  <td>2000</td>
  <td>2002</td>
  <td>Bethseda Softworks</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Prince of Persia: Sands of Time</td>
  <td>2000</td>
  <td>2003</td>
  <td>Ubisoft</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Cave Story</td>
  <td>2000</td>
  <td>2004</td>
  <td>Studio Pixel</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Sacred Gold</td>
  <td>2000</td>
  <td>2004</td>
  <td>Ascaron</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Vampire: The Masquerade - Bloodlines</td>
  <td>2000</td>
  <td>2004</td>
  <td>Troika Games</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Star Wars: KOTOR 2</td>
  <td>2000</td>
  <td>2005</td>
  <td>Obsidian Entertainment</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Bioshock</td>
  <td>2000</td>
  <td>2007</td>
  <td>2K</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Bioshock Remastered</td>
  <td>2000</td>
  <td>2007</td>
  <td>2K</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Broken Sword Director´s Cut</td>
  <td>2000</td>
  <td>2008</td>
  <td>Revolution</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Braid</td>
  <td>2000</td>
  <td>2009</td>
  <td>Number None</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Machinarium</td>
  <td>2000</td>
  <td>2009</td>
  <td>Amanita Design</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Batman: Arkham Asylum</td>
  <td>2010</td>
  <td>2010</td>
  <td>Rocksteady Studios</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Bioshock 2</td>
  <td>2010</td>
  <td>2010</td>
  <td>2K</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Bioshock 2 Remastered</td>
  <td>2010</td>
  <td>2010</td>
  <td>2K</td>
  <td>Steam</td>
</tr>
<tr>
  <td>LIMBO</td>
  <td>2010</td>
  <td>2010</td>
  <td>Playdead</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Bastion</td>
  <td>2010</td>
  <td>2011</td>
  <td>Supergiant Games</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Portal 2</td>
  <td>2010</td>
  <td>2011</td>
  <td>Valve Corporation</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Spacechem</td>
  <td>2010</td>
  <td>2011</td>
  <td>Zachotronics</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Superbrothers: Sword &amp; Sworcery</td>
  <td>2010</td>
  <td>2011</td>
  <td>Superbrothers</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Dark Souls</td>
  <td>2010</td>
  <td>2012</td>
  <td>From</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Dear Esther</td>
  <td>2010</td>
  <td>2012</td>
  <td>The Chinese Room</td>
  <td>Steam</td>
</tr>
<tr>
  <td>FEZ</td>
  <td>2010</td>
  <td>2012</td>
  <td>Polytron Corporation</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Hotline: Miami</td>
  <td>2010</td>
  <td>2012</td>
  <td>Dennaton Games</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Spec Ops: The Line</td>
  <td>2010</td>
  <td>2012</td>
  <td>Yager Development</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Thirty Flights of Loving</td>
  <td>2010</td>
  <td>2012</td>
  <td>Blendo Games</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Thomas Was Alone</td>
  <td>2010</td>
  <td>2012</td>
  <td>Mike Bithell</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Antichamber</td>
  <td>2010</td>
  <td>2013</td>
  <td>Demruth</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Bioshock Infinite</td>
  <td>2010</td>
  <td>2013</td>
  <td>Irrational Games</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Gone Home</td>
  <td>2010</td>
  <td>2013</td>
  <td>The Fullbright Company</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Kentucky Route Zero</td>
  <td>2010</td>
  <td>2013</td>
  <td>Cardboard Computer</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Papers, Please</td>
  <td>2010</td>
  <td>2013</td>
  <td>Lucas Pope</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Proteus</td>
  <td>2010</td>
  <td>2013</td>
  <td>Curve Digital</td>
  <td>Steam</td>
</tr>
<tr>
  <td>The Stanley Parable</td>
  <td>2010</td>
  <td>2013</td>
  <td>Galactic Cafe</td>
  <td>Steam</td>
</tr>
<tr>
  <td>The Talos Principle</td>
  <td>2010</td>
  <td>2014</td>
  <td>Croteam</td>
  <td>Steam</td>
</tr>
<tr>
  <td>The Vanishing of Ethan Carter</td>
  <td>2010</td>
  <td>2014</td>
  <td>The Astronauts</td>
  <td>Steam</td>
</tr>
<tr>
  <td>80 days</td>
  <td>2010</td>
  <td>2015</td>
  <td>Inkle</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Aviary Attorney</td>
  <td>2010</td>
  <td>2015</td>
  <td>Sketchy Logic</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Else Heart.Break()</td>
  <td>2010</td>
  <td>2015</td>
  <td>Erik Svedang</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Fahrenheit</td>
  <td>2010</td>
  <td>2015</td>
  <td>Quantic Dream</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Her Story</td>
  <td>2010</td>
  <td>2015</td>
  <td>Sam Barlow</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Kingdom: Classic</td>
  <td>2010</td>
  <td>2015</td>
  <td>Noio, Licorice</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Mini Metro</td>
  <td>2010</td>
  <td>2015</td>
  <td>Dinosaur Polo Club</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Pathologic Classic HD</td>
  <td>2010</td>
  <td>2015</td>
  <td>Icepick Lodge</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Sunless Sea</td>
  <td>2010</td>
  <td>2015</td>
  <td>Failbetter Games</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Undertale</td>
  <td>2010</td>
  <td>2015</td>
  <td>Toby Fox</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Beholder</td>
  <td>2010</td>
  <td>2016</td>
  <td>Warm Lamp Games</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Dark Train</td>
  <td>2010</td>
  <td>2016</td>
  <td>Paperash Studio</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Darkest Dungeon</td>
  <td>2010</td>
  <td>2016</td>
  <td>Red Hook Studios</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Firewatch</td>
  <td>2010</td>
  <td>2016</td>
  <td>Campo Santo</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Hyper Light Drifter</td>
  <td>2010</td>
  <td>2016</td>
  <td>Heart Machine</td>
  <td>Steam</td>
</tr>
<tr>
  <td>INSIDE</td>
  <td>2010</td>
  <td>2016</td>
  <td>Playdead</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Oxenfree</td>
  <td>2010</td>
  <td>2016</td>
  <td>Night School Studio</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Reigns</td>
  <td>2010</td>
  <td>2016</td>
  <td>Nerial</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Shadow Tactics: Blades of the Shogun</td>
  <td>2010</td>
  <td>2016</td>
  <td>Mimimi Productions</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Street Figher V</td>
  <td>2010</td>
  <td>2016</td>
  <td>Capcom</td>
  <td>Steam</td>
</tr>
<tr>
  <td>SUPERHOT VR</td>
  <td>2010</td>
  <td>2016</td>
  <td>SUPERHOT Team</td>
  <td>Steam</td>
</tr>
<tr>
  <td>The Westport Independent</td>
  <td>2010</td>
  <td>2016</td>
  <td>Double Zero One Zero</td>
  <td>Steam</td>
</tr>
<tr>
  <td>This is Police</td>
  <td>2010</td>
  <td>2016</td>
  <td>Weappy Studio</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Virginia</td>
  <td>2010</td>
  <td>2016</td>
  <td>Variable State</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Bayonetta</td>
  <td>2010</td>
  <td>2017</td>
  <td>Platinum</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Dujanah</td>
  <td>2010</td>
  <td>2017</td>
  <td>Jack King-Spooner</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Hidden Folks</td>
  <td>2010</td>
  <td>2017</td>
  <td>Adriaan De Jongh</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Hollow Knight</td>
  <td>2010</td>
  <td>2017</td>
  <td>Team Cherry</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Night in the Woods</td>
  <td>2010</td>
  <td>2017</td>
  <td>Infinite Fall</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Old Man&#39;s Journey</td>
  <td>2010</td>
  <td>2017</td>
  <td>Broken Rules</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Sluggish Morsse: days of the Purple</td>
  <td>2010</td>
  <td>2017</td>
  <td>Jack King-Spooner</td>
  <td>Steam</td>
</tr>
<tr>
  <td>The Norwood Suite</td>
  <td>2010</td>
  <td>2017</td>
  <td>Cosmo D</td>
  <td>Steam</td>
</tr>
<tr>
  <td>What Remains of Edith Finch</td>
  <td>2010</td>
  <td>2017</td>
  <td>Giant Sparrow</td>
  <td>Steam</td>
</tr>
<tr>
  <td>FAR: Lone Sails</td>
  <td>2010</td>
  <td>2018</td>
  <td>Okomotive</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Frostpunk</td>
  <td>2010</td>
  <td>2018</td>
  <td>11 bit studios</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Kingdome Come: Deliverance</td>
  <td>2010</td>
  <td>2018</td>
  <td>Warhorse Studios</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Return of the Obra Dinn</td>
  <td>2010</td>
  <td>2018</td>
  <td>Lucas Pope</td>
  <td>Steam</td>
</tr>
<tr>
  <td>A Plague Tale: Innocence</td>
  <td>2010</td>
  <td>2019</td>
  <td>Asobo Studio</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Beat Saber</td>
  <td>2010</td>
  <td>2019</td>
  <td>Beat Games</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Disco Elysium</td>
  <td>2010</td>
  <td>2019</td>
  <td>ZA/UM</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Flower</td>
  <td>2010</td>
  <td>2019</td>
  <td>thatgamecompany</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Overland</td>
  <td>2010</td>
  <td>2019</td>
  <td>Finjii</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Pathologic 2</td>
  <td>2010</td>
  <td>2019</td>
  <td>Ice pick lodge</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Telling Lies</td>
  <td>2010</td>
  <td>2019</td>
  <td>Sam Barlow</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Beyond Two Souls</td>
  <td>2020</td>
  <td>2020</td>
  <td>Quantic Dream</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Death Stranding</td>
  <td>2020</td>
  <td>2020</td>
  <td>KOJIMA PRODUCTIONS</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Detroit</td>
  <td>2020</td>
  <td>2020</td>
  <td>Quantic Dream</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Factorio</td>
  <td>2020</td>
  <td>2020</td>
  <td>Wube Software LTD.                                                                        </td>
  <td>Steam</td>
</tr>
<tr>
  <td>Florence</td>
  <td>2020</td>
  <td>2020</td>
  <td>Mountains</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Half Life Alyx</td>
  <td>2020</td>
  <td>2020</td>
  <td>Valve</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Heavy Rain</td>
  <td>2020</td>
  <td>2020</td>
  <td>Quantic Dream</td>
  <td>Steam</td>
</tr>
<tr>
  <td>In other waters</td>
  <td>2020</td>
  <td>2020</td>
  <td>Jump Over The Age</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Journey</td>
  <td>2020</td>
  <td>2020</td>
  <td>thatgamecompany</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Ministry of Broadcast</td>
  <td>2020</td>
  <td>2020</td>
  <td>Ministry of Broadcast Studios</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Outer Wilds</td>
  <td>2020</td>
  <td>2020</td>
  <td>Mobius Digital</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Untitled Goose Game</td>
  <td>2020</td>
  <td>2020</td>
  <td>House House</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Death Trash </td>
  <td>2020</td>
  <td>2021</td>
  <td>Crafting Legends</td>
  <td>Steam</td>
</tr>
<tr>
  <td>Sable</td>
  <td>2020</td>
  <td>2021</td>
  <td>Shedworks</td>
  <td>Steam</td>
</tr>
</table>

### PC - GOG

### PlayStation

<table>
<tr>
<th>Název hry</th><th>Dekáda</th><th>Rok vydání</th><th>Autor / Studio</th><th>Platforma</th>
</tr>
<tr>
<td>Death Stranding</td>
<td>2020</td>
<td></td>
<td>Kojima Studios</td>
<td>Fzyická kopie</td>
</tr>
</table>