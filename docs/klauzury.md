---
sidebar_position: 3
---

# Klauzury

## Jaká jsou klauzurní zadání?

### 1. ročník - Herní prototyp

### 2. ročník - Absolventská hra

## Co a jak je potřeba odevzdat?

Při odevzdávání klauzurní práce odevzdávejte níže popsaný balík souborů. Hlavní materiály je nutné **do pulnoci (23:59) v den deadlinu odevzdat na e-mail hernidesign@famu.cz** pomocí služby jako je například WeTransfer. Soubory neodevzdávejte jako přílohu e-mailu.

Prezentační materiály odevzdávejte stejným způsobem jako hlavní materiály, a to **do pulnoci (23:59) 3 dny před konáním klauzur**.

## Deadliny 2024

### 1. ročník - Herní prototyp

- **Hlavní materiály** - 14.6.2024 do 23:59
- **Prezentační materiály** - 21.6.2024 do 23:59

### 2. ročník - Absolventská hra - jarní termín

- **Hlavní materiály** - 10.6.2024 do 23:59
- **Prezentační materiály** - 17.6.2024 do 23:59

### 2. ročník - Absolventská hra - podzimní termín

- **Hlavní materiály** - TBD
- **Prezentační materiály** - TBD

## Popis 
### Hlavní materiály

- build hry hratelný na operačním systému Windows
- autorskou explikaci ve formátu PDF o rozsahu 1 - 2 normostrany (1800 - 3600 znaků)
    - autorská explikace má za účel přiblížit klauzurní komisi vaše východiska, tvůrčí proces a cíle
    - explikace by měla popisovat současný stav díla, nepopisujte budoucí plány
    - v explikaci není nutné dopodrobna popisovat děj nebo průchod hrou 
    - v explikaci je povinné uvést titulkovou listinu, tzn. seznam lidí, kteří se na hře podíleli a co bylo náplní jejich práce
- gameplay video ve formátu MP4 (pro pořízení doporučujeme využít program OBS Studio)
    - délka gameplay videa není omezena, ale mělo by zachytit všechny hlavní prvky vaší hry, ať už se jedná o mechaniky, art direction, sound design, atd.

### Prezentační materiály

- anotaci
    - anotace je krátký popisek, který budeme využívat na webových stránkách a v jiných prezentačních materiálech
    - jedná sa o "reklamní" text, způsob, jakým jej napíšete, je čistě na vás
    - anotace by měla mít **maximálne 500 znaků**
- minimálně 5 screenshotu ze hry ve formátu PNG
    - jeden ze screenshotu můžete označit jako "hlavní", budeme ho pak používat jako thumbnail
- trailer ve formátu MP4 (maximální délka 2 minuty)
- build hry hratelný na operačním systému Windows (pro věřejné prezentace, může obsahovat změny oproti klauzurnímu buildu)

## Jak probíhají klauzury?

### Obecně

Klauzury jsou primárně individuální prezentace každé práce a diskuse studentstva a komise. Při individuální prezentaci je možné využít gameplay videa, traileru nebo předpřipravené prezentace odvíjející se od autorské explikace.

## Klauzury 2024

### 1. ročník

- Datum: 24.6.2024
- Program
    - Klimentská 6
        - 10:00 - 14:00 - prezentace her a otevřená diskuse nad každou prací
        - 14:00 - 14:30 - vyhodnocení

### 2. ročník - jarní termín

- Datum: 20.6.2024
- Program
    - Klimentská 6
        - StátniceTBD
        - Klauzury TBD

### 2. ročník - podzimní termín

- Datum: TBD
- Program
    - TBD

## Prezentace pro věřejnost 2024

- Datum: 24.6.2024

TBD Michal
