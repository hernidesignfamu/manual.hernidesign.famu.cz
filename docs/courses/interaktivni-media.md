---
sidebar_position: 6
---

# Teorie interaktivních médií

## Info

** Učebna HD (Klimentská 6) **

** ZS, každou středu 15:40 - 17:15 **

** Vyučující: Andrej Sýkora (<andrej.sykora@famu.cz>) **

## Harmonogram

- #1 Archív (2.10.2024)
- NEKONÁ SE (9.10.2024)
- #2 Metafora (16.10.2024)
    - Text: [Wendy Hui Kyong Chun - Computers That Roar](/files/courses/interaktivni-media/Wendy_Hui_Kyong_Chun__Computers_That_Roar.pdf)
- #3 Algoritmus (30.10.2024)
    - Softvér: [Kate Compton - Tracery](https://theawesomefufuman.github.io/Tracitor/)
- #4 Databáza (6.11.2024)
    - Videohra: [Tim Sheinman - Family](https://tim-sheinman.itch.io/family)
- #S Seminár 1 (13.11.2024)
    - Prosím, doneste si na hodinu krátku prezentáciu ľubovoľného umeleckého diela, fenoménu, technológie, myšlienky a podobne.
    - Základným predpokladom je, že je to niečo, čo naozaj *chcete* ostatným ukázať.
    - Je vítané, ak je tento objekt nejak prepojený s interaktívnymi médiami obecne, ale nie je to podmienkou.
    - Pokúste sa vo vašej prezentácií popísať rôzne aspekty objektu, ktorý ste doniesli.
- #6 Príbeh (20.11.2024)
- #7 Moc (27.11.2024)
    - Videohry:
        - [bloodrizer - Kittens Game](http://kittensgame.com/web/)
        - [Frankt Lantz - Universal Paperclips](https://www.decisionproblem.com/paperclips/)
- #8 Láska (11.12.2024)
    - Videohra:
        - [Paper Dino Software - Save The Date](https://paperdino.com/save-the-date/)
- #S Seminár 2 (18.12.2024)
    - Deskovka: [Everest Pipkin - Ground Itself](https://everestpipkin.itch.io/the-ground-itself), [PDF - Pravidla](https://drive.google.com/drive/folders/1ktqFVG3hmk-cLS_P89bWRh-C-4Zmd72b?usp=sharing)

## Atestace

- docházka 75%
- účast na obou seminářích (13.11.2024 a 18.12.2024)

## Zdroje

### #1 Archív

**Media**
- [The Goggles - Welcome to Pine Point](https://pinepoint.nfb.ca/)
- [Richard Cloutier - Pine Point Revisited](http://pinepointrevisited.homestead.com/)
- [Joey Schutz - Maps Of The Known World](https://jamschutz.itch.io/maps-of-the-known-world)
- [Amalia Ulman - Excellences & Perfections](https://webenact.rhizome.org/excellences-and-perfections/)
- [PUP - Pine Point](https://www.youtube.com/watch?v=bi0LjAPFrMo)

### #2 Metafora

**Media**
- [Jason Rohrer - Passage](http://hcsoftware.sourceforge.net/passage/)
- [Jonathan Blow - The Witness](https://store.steampowered.com/app/210970/The_Witness/)
    - [Let's Play](https://www.youtube.com/watch?v=gGenrgmMcZc)

**Books**
- [J.D.Bolter and R.Grusin - Remediation: Understanding New Media](https://monoskop.org/images/a/ae/Bolter_Jay_David_Grusin_Richard_Remediation_Understanding_New_Media_low_quality.pdf)
- [Adam Greenfield - Radical Technologies: The Design of Everyday Life](https://www.amazon.com/Radical-Technologies-Design-Everyday-Life/dp/178478043X)

### #3 Algoritmus

**Media**
- [Jon Rafman - Counterfeit Poast (14:30)](https://dis-art.libproxy.mit.edu/counterfeit-poast)

**Books**
- [Mark R. Johnson - The Unpredictability of Gameplay](https://www.bloomsbury.com/us/unpredictability-of-gameplay-9781501321603/)
- [Matteo Pasquinelli - The Eye of the Master](https://www.versobooks.com/products/735-the-eye-of-the-master)
- [James Bridle - New Dark Age](https://www.hostbrno.cz/temne-zitrky/)

### #4 Databáza

**Media**
- [Census Americans](https://twitter.com/censusamericans)
- [Darius Kazemi - Last Words](http://tinysubversions.com/stuff/lastwords/)
- [Darius Kazemi - All Projects](http://tinysubversions.com/projects/)
- [TV Tropes](https://tvtropes.org/)
- [Pippin Barr - v3](https://pippinbarr.com/v-r-3/info/)
- [Pippin Barr - v-r-49](https://pippinbarr.com/v-r-4-99/info/)

**Books**
- []

<!--

### #5 Moc

**Media**

- [Richard Hofmeier - Cart Life](https://www.youtube.com/watch?v=1of0xqJ_aNI)
- [Paolo Pedercini - Lichenia](http://www.molleindustria.org/lichenia/)
- [Paolo Pedercini - Democratic Socialism Simulator](https://molleindustria.itch.io/democratic-socialism-simulator)

### #6 Láska -->
