---
sidebar_position: 4
---

# Kapitoly z herního designu

## Harmonogram

## LS 2025

<table>
    <tr>
        <th>Datum</th>
        <th>Host</th>
        <th>Téma</th>
    </tr>
    <tr>
        <td>11.2</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>18.2</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>25.2</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>4.3</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>11.3</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>18.3</td>
        <td>Martin Ziegler (Lead Scripter @ Warhorse Studios)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>25.3</td>
        <td>Martin Ziegler (Lead Scripter @ Warhorse Studios)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>1.4</td>
        <td>TBA</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>8.4</td>
        <td>Pavel Mironov</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>15.4</td>
        <td>Pavel Mironov</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>22.4</td>
        <td>TBA</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>29.4</td>
        <td>TBA</td>
        <td>TBA</td>
    </tr>
</table>

## Archiv

### ZS 2024

<table>
    <tr>
        <th>Datum</th>
        <th>Host</th>
        <th>Téma</th>
    </tr>
    <tr>
        <td>1.10</td>
        <td>Úvod do studia pro studenty KHD</td>
        <td>-</td>
    </tr>
    <tr>
        <td>8.10</td>
        <td>NEKONÁ SE / PONĚŠICE KHD</td>
        <td>-</td>
    </tr>
    <tr>
        <td>15.10</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>22.10</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>29.10</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>5.11</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>12.11</td>
        <td>Martin Ziegler (Lead Scripter @ Warhorse Studios)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>19.11</td>
        <td>Martin Ziegler (Lead Scripter @ Warhorse Studios)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>26.11</td>
        <td>Lenka Krsová</td>
        <td>Level Design</td>
    </tr>
    <tr>
        <td>3.12</td>
        <td>Ivan Buchta (Creative Director @ Bohemia Interactive)</td>
        <td>Level Design</td>
    </tr>
    <tr>
        <td>10.12</td>
        <td>Quinten Buijs</td>
        <td>Level Design</td>
    </tr>
    <tr>
        <td>17.12</td>
        <td>Studenti KHD</td>
        <td>Prezentace prváckých a absolventských her</td>
    </tr>
</table>

### LS 2024

<table>
    <tr>
        <th>Datum</th>
        <th>Host</th>
        <th>Téma</th>
    </tr>
    <tr>
        <td>13.2</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>20.2</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>27.2</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>5.3</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>12.3</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>19.3</td>
        <td>Martin Ziegler (Lead Scripter @ Warhorse Studios)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>26.3</td>
        <td>Martin Ziegler (Lead Scripter @ Warhorse Studios)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>2.4</td>
        <td>TBA</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>9.4</td>
        <td>Jan Ilavský (Founder @ Beat Games)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>16.4</td>
        <td>Pavel Tovaryš (Founder @ Rake in Grass)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>23.4</td>
        <td>Anton Romanenko (Designer @ SCS Software)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>30.4</td>
        <td>Jakub Dvorský (Founder @ Amanita Design)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>7.5</td>
        <td>TBA</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>14.5</td>
        <td>TBA</td>
        <td>TBA</td>
    </tr>
</table>

### ZS 2023

<table>
    <tr>
        <th>Datum</th>
        <th>Host</th>
        <th>Téma</th>
    </tr>
    <tr>
        <td>3.10</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>10.10</td>
        <td>NEKONÁ SE / PONĚŠICE KHD</td>
        <td>-</td>
    </tr>
    <tr>
        <td>17.10</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>24.10</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>31.10</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>7.11</td>
        <td>Martin Ziegler (Lead Scripter @ Warhorse Studios)</td>
        <td>Hloubková analýza</td>
    </tr>
    <tr>
        <td>14.11</td>
        <td>Martin Ziegler (Lead Scripter @ Warhorse Studios)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>21.11 (start 16:00)</td>
        <td>Martin Schlichter</td>
        <td>Making cards great again since 14. century</td>
    </tr>
    <tr>
        <td>28.11</td>
        <td>Volha Kapitonava (Project Manager @ Threaks, prev. Community Lead @ ZA/UM)</td>
        <td>Intro to Crafting a Powerful Online Presence for Your Game</td>
    </tr>
    <tr>
        <td>5.12</td>
        <td>Quinten Buijs (Freelance Game Designer)</td>
        <td>Level Design</td>
    </tr>
    <tr>
        <td>12.12</td>
        <td>Ivan Buchta (Creative Director @ Bohemia Interactive)</td>
        <td>Design rozsáhlých světů</td>
    </tr>
    <tr>
        <td>19.12</td>
        <td>Studenti herního designu</td>
        <td>Prezentace konceptů prototypu 1. ročníku</td>
    </tr>
</table>

### LS 2023

<table>
    <tr>
        <th>Datum</th>
        <th>Host</th>
        <th>Téma</th>
    </tr>
    <tr>
        <td>14.2</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>21.2</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>28.2</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>7.3</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>14.3</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>21.3</td>
        <td>Martin Ziegler (Lead Scripter @ Warhorse Studios)</td>
        <td>Hloubková analýza - Slay the Spire</td>
    </tr>
    <tr>
        <td>28.3</td>
        <td>Martin Ziegler (Lead Scripter @ Warhorse Studios)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>4.4</td>
        <td>Ondřej Trhoň (No Fun Kolektiv, Charles Games)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>11.4</td>
        <td>Martin Hájek (Game Designer @ Wargaming)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>18.4</td>
        <td>Michal Roch (CEO @ Honestly Games)</td>
        <td>Indie akcelerátor a provoz herního studia</td>
    </tr>
    <tr>
        <td>25.4</td>
        <td>-</td>
        <td>Nekoná se</td>
    </tr>
    <tr>
        <td>2.5</td>
        <td>Tomáš Roller (Plan A)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>10.5 o 11:00</td>
        <td>Jarek Kolář</td>
        <td>Přednáška + Workshop</td>
    </tr>
</table>

### ZS 2022

<table>
    <tr>
        <th>Datum</th>
        <th>Host</th>
        <th>Téma</th>
    </tr>
    <tr>
        <td>4.10</td>
        <td>PONĚŠICE (jen pro studenty KHD)</td>
        <td>Úvod do studia</td>
    </tr>
    <tr>
        <td>11.10</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>18.10</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>25.10</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>1.11</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>8.11</td>
        <td>Martin Ziegler (Lead Scripter @ Warhorse Studios)</td>
        <td>Procedurálně generovaný obsah</td>
    </tr>
    <tr>
        <td>15.11</td>
        <td>Martin Ziegler (Lead Scripter @ Warhorse Studios)</td>
        <td>Hloubková / komparativní analýza</td>
    </tr>
    <tr>
        <td>22.11</td>
        <td>Sergio Massabo (Producer)</td>
        <td>Produkce lokalizovaného obsahu</td>
    </tr>
    <tr>
        <td>29.11</td>
        <td>Ivan Buchta (Creative Director @ Bohemia Interactive)</td>
        <td>Design rozsáhlých světu</td>
    </tr>
    <tr>
        <td>6.12</td>
        <td>TBA</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>13.12</td>
        <td>Studenti KHD (FAMU)</td>
        <td>Prezentace - Koncepty prototypu z Dílny 1</td>
    </tr>
    <tr>
        <td>20.12</td>
        <td>NEKONÁ SE</td>
        <td>-</td>
    </tr>
</table>


### ZS 2021

<table>
    <tr>
        <th>Datum</th>
        <th>Host</th>
        <th>Téma</th>
    </tr>
    <tr>
        <td>5.10</td>
        <td>Michal Berlinger (Game Developer @ Amanita Design)</td>
        <td>Case Study - Pilgrims</td>
    </tr>
    <tr>
        <td>12.10</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>19.10</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>26.10</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>2.11</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Systémový design</td>
    </tr>
    <tr>
        <td>9.11</td>
        <td>Ivan Buchta (Creative Director @ Bohemia Interactive)</td>
        <td>Design rozsáhlých světu</td>
    </tr>
    <tr>
        <td>16.11</td>
        <td>Martin Hájek (Game Designer @ Bohemia Interactive)</td>
        <td>Design herního obsahu (singleplayer vs. coop vs. multiplayer)</td>
    </tr>
    <tr>
        <td>23.11</td>
        <td>Jakub Dvorský (Amanita Design)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>30.11</td>
        <td>Radim Jurda a Jan Chlup (Amanita Design)</td>
        <td>Case Study - Creaks</td>
    </tr>
    <tr>
        <td>7.12</td>
        <td>Jára Plachý (Amanita Design)</td>
        <td>Case Study - Happy Game</td>
    </tr>
    <tr>
        <td>14.12</td>
        <td>Studenti KHD (FAMU)</td>
        <td>Prezentace - Koncepty prototypu z Dílny 1</td>
    </tr>
</table>

### LS 2022

<table>
    <tr>
        <th>Datum</th>
        <th>Host</th>
        <th>Téma</th>
    </tr>
    <tr>
        <td>15.2</td>
        <td>Vojtěch Leischner (ČVUT)</td>
        <td>Interaktivita ve fyzickém prostoru</td>
    </tr>
    <tr>
        <td>22.2</td>
        <td>Lubor Kopecký</td>
        <td>Úvod do produkce a distribuce her</td>
    </tr>
    <tr>
        <td>1.3</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Statistika a pravděpodobnost v herním designu</td>
    </tr>
    <tr>
        <td>8.3</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Modelování RPG mechanik</td>
    </tr>
    <tr>
        <td>15.3</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Náčrt bojového systému</td>
    </tr>
    <tr>
        <td>22.3</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>Designovými podvody za lepší gameplay</td>
    </tr>
    <tr>
        <td>29.3</td>
        <td>Viktor Bocan (Design Director @ Warhorse Studios)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>5.4</td>
        <td>Michal Kovařík (Technology Director @ Wube Software)</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>12.4</td>
        <td>Nekoná se</td>
        <td></td>
    </tr>
    <tr>
        <td>19.4</td>
        <td>Nekoná se</td>
        <td></td>
    </tr>
    <tr>
        <td>26.4</td>
        <td>Ondřej Trhoň a Barbora Tauerová</td>
        <td>Videoherní architektura</td>
    </tr>
    <tr>
        <td>3.5</td>
        <td>Jaroslav Meloun (Rendlike)</td>
        <td>FixFox - Case Study</td>
    </tr>
    <tr>
        <td>10.5</td>
        <td>TBA</td>
        <td>TBA</td>
    </tr>
    <tr>
        <td>17.5</td>
        <td>CBE (speaker TBA)</td>
        <td>TBA</td>
    </tr>
</table>
