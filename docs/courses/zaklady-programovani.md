---
sidebar_position: 2
---

# Základy programování

## Harmonogram

1. Úvodní hodina
    - Visual Studio
    - Vytvoření konzolové aplikace
    - Vstup / výstup z konzole
2. Datové typy, základní operace a řídící struktury
    - datové typy
        - bool
        - int
        - float / double
        - string
    - náhoda
        - new Random()
    - operace
        - aritmetické operace
            - +, -, *, /
        - logické operace
            - ==, !=, >, <, &&, ||
    - řídící struktury
        - if / else
        - while
3. Funkce a metody, struktura projektu, switch, game loop
    - namespace a class
    - funkce
    - definice funkcí / metod
        - datový typ návratové hodnoty a void
        - parametry funkce / metody
        - tělo funkce / metody
    - řídící struktury
        - switch
    - koncepty
        - game loop
4. Funkce / Metody, základy tříd
    - třídy
        - statické vlastnosti a metody
        - instance a dědění
        - vlastnosti a metody - public, protected, private

## Sylabus

Níže je uvedený seznam konceptů, které by jste měli na konci semestru znát.

- vytvoření konzolové aplikace v prostředí Visual Studio
- vstup a výstup z konzole (Console.WriteLine, Console.ReadLine)
- datové typy
    - bool
    - int
    - float / double
    - string
- proměnné
- operace
    - aritmetické operace
        - +, -, *, /
    - logické operace
        - ==, !=, >, <, &&, ||
- řídící struktury
    - if / else
    - while
    - foreach
    - try / catch
- třídy (class)
    - definice tříd
    - rozdíl mezi statickými a nestatickými metodami a vlastnostmi
    - konstruktory
    - metody / funkce
        - definice metody
        - parametry metody
        - návratové hodnoty
    - abstraktní třídy
    - dědičnost
    - reflexe
- rozhraní (interface)
- standardní knihovna
    - náhoda
        - Random
    - seznamy
        - List
    - slovníky
        - Dictionary
- koncepty
    - game loop pomocí while
    - příkazy a jejich vyhodnocování
    - vykreslování mapy a 2D čtveřečková mapa obecně
    - způsob implementace inventáře nebo seznamu postav / lokací

## Příklady

Základní kód pro konzolovou aplikaci

```
using System;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
```

Jednoduché "renderování" mapy na základě velikosti mapy a pozice hráče:

```
static void DrawMap(int width, int height, int player_x, int player_y)
{
    string render = "";
    int renderY = 0;

    while (renderY < height)
    {
        int renderX = 0;

        while (renderX < width)
        {
            if (renderX == player_x && renderY == player_y)
            {
                render += "@";
            } else
            {
                render += "#";
            }                    
            renderX = renderX + 1;
        }

        render += "\n";
        renderY = renderY + 1;
    }

    Console.WriteLine(render);
}
```

Renderování spritu z tilemapy v SFML:

```
using SFML.Graphics;
using SFML.Window;

namespace SFMLSpriteExample
{
    class Program
    {
        static string TILEMAP_PATH = "assets/colored_packed.png";

        static Texture LoadTextureFromTileMap (int posX, int posY)
        {
            IntRect area = new IntRect(posX * 16, posY * 16, 16, 16);
            return new Texture(TILEMAP_PATH, area);
        }

        static void Main(string[] args)
        {
            string[] arr = {"a", "b"};

            foreach (string a in arr) {
                Console.WriteLine(a);
            }

            InitGfx();
        }

        static void InitGfx ()
        {
            var w = new RenderWindow(new VideoMode(800, 600), "Demo");
            var shape = new CircleShape();
            shape.FillColor = Color.Blue;

            Sprite b = new Sprite(LoadTextureFromTileMap(0, 1));
            b.Position = new SFML.System.Vector2f(32, 32);

            while (w.IsOpen)
            {
                w.DispatchEvents();
                w.Clear(Color.Black);
                w.Draw(b);
                w.Display();
            }
        }
    }
}
```

## Zápočtový projekt

Konzolová aplikace - textová hra, která implementuje následující funkcionality:

### Společné zadání

- 2D mapa (grid) a pohyb hráče po ní
    - příkazy "sever", "jih", "zapad", "vychod" (resp. "s", "j", "z", "v")
    - hláška informující hráče, že v daném směru nelze vykonat pohyb, pokud je na kraji mapy
- hru lze "dohrát", tzn. existuje nějaká podmínka vítězství (exit, získání pokladu a exit, etc.)
- inventář
- NPCs
- místnosti obsahují předměty a NPCs 
