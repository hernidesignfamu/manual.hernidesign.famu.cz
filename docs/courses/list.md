---
sidebar_position: 1
---

# Seznam předmětů

## Povinné předměty

### Dílny

- [Dílna 1](https://sp.amu.cz/cs/predmet312DN1.html)
- [Dílna 2](https://sp.amu.cz/cs/predmet312DN2.html)
- [Dílna 3](https://sp.amu.cz/cs/predmet312DN3.html)
- [Dílna 4](https://sp.amu.cz/cs/predmet312DN4.html)

### Praktické předměty

- [Základy programování](zaklady-programovani)
- [Middleware 1](https://sp.amu.cz/cs/predmet312MW1.html)
- [Middleware 2](https://sp.amu.cz/cs/predmet312MW2.html)
- [Middleware 3](https://sp.amu.cz/cs/predmet312MW3.html)
- [Kapitoly z herního designu](kapitoly-z-herniho-designu)
- [Vývoj PC her](https://sp.amu.cz/cs/predmet312VPH.html)
- [Stáž v herním studiu](https://sp.amu.cz/cs/predmet312SHS.html)

### Teoretické předměty

- [Digital Game Histories](https://sp.amu.cz/cs/predmet312DGH.html)
- [Herní teorie 1](https://sp.amu.cz/cs/predmet312HT1.html)
- [Herní teorie 2](https://sp.amu.cz/cs/predmet312HT2.html)
- [Seminář analýzy PC her](https://sp.amu.cz/cs/predmet312SAPH.html)

## Povinně-volitelné předměty

- [Herní scenáristika 1](https://sp.amu.cz/cs/predmet312HS1.html)
- [Herní scenáristika 2](https://sp.amu.cz/cs/predmet312HS2.html)
- [Interaktivní zvuková tvorba 1](https://sp.amu.cz/cs/predmet312ZIZT1.html)
- [Interaktivní zvuková tvorba 2](https://sp.amu.cz/cs/predmet312ZIZT2.html)
- [Animace a grafika v PC hrách 1](https://sp.amu.cz/cs/predmet312AGPH1.html)
- [Animace a grafika v PC hrách 2](https://sp.amu.cz/cs/predmet312AGPH2.html)

## Volitelné předměty

- [Teorie interaktivních médií](interaktivni-media)
- [Počítačové hry, společnost a reprezentace](https://sp.amu.cz/cs/predmet312PHSR.html)
- [Game art a artové hry](https://sp.amu.cz/cs/predmet312GAAH.html)
- [Produkce a distribuce her 1](https://sp.amu.cz/cs/predmet312PDH1.html)
- [Produkce a distribuce her 2](https://sp.amu.cz/cs/predmet312PDH2.html)