---
sidebar_position: 6
---

# Publishing

FAMU od února 2025 umožňuje komercionalizovat studentské projekty, ať už jde o herní prototypy 1. ročníku, nebo absolventské hry.

Komercionalizovat je možné i retrospektivně jako absolvent, stačí podepsat absolventskou verzi smlouvy.

## Podmínky komercionalizace

- smlouva je napsána velice svobodně a týká se jen příjmu studenta/ky jako fyzické osoby která je autorem díla
- nezasahuje tak do žádných dalších vztahů s distributorama, vydavateli, spolupracovníky a podobně
- AMU si nárokuje 30 % + DPH (celkové zatížení je tak pro neplátce DPH 36,3 %), pokud příjem za rok překročí 30 000,- korun
- nárok po 5 letech zaniká
- report o příjmech stačí zaslat jednou ročně, a to do 31.3 následujícího kalendářního roku
- report není třeba zaslat, pokud příjem za daný rok nepřekročil 30 000,- korun

## Vzory smluv

[Smlouva pro studenty](https://docs.google.com/document/d/1NshTMfrnBOkDo6LgmD7CYmuEeg8v6nQh/edit?usp=sharing&ouid=110076813591903663396&rtpof=true&sd=true)

[Smlouva pro absolventy](https://docs.google.com/document/d/1sXXdMkvUbs2XbCBASoHU-ZcGUdFKgKRW/edit?usp=sharing&ouid=110076813591903663396&rtpof=true&sd=true)

## Pomoc

V případě jakýchkoliv dalších dotazů nebo nejasností kontaktuje vedoucího katedry na Discordu nebo e-mailem.