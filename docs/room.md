---
sidebar_position: 4
---

# Učebna a vybavení

KHD momentálně na většinu své výuky využívá učebnu v Klimentské 6.

## Wifi

K dispozici je Wifi "GD-KLIM6" a "GD-KLIM5G", heslo je "famuklim6".

## Vybavení

### Učebna - PC

Učebna je vybavena 9 pracovními stanicemi (8x student, 1x pedagog) v těchto konfiguracích:

#### Studentské PC

- Intel Core i7-10700F
- 16GB DDR4
- NVIDIA RTX3060
- 1TB SSD

#### Ludotéka PC

- Intel Core i5
- 32GB DDR4
- NVIDIA RTX3070
- 1TB SSD

#### Pedagogické PC

- Intel Core i5
- 32GB DDR4
- NVIDIA RTX3060
- 1TB SSD

### Učebna - Periferie

V učebně je dále k dispozici:

- FullHD projekce
- 2.0 stereo reprosoustava
- 8x sluchátka
- 1x USB prezentér
- 4K televize Samsung
- arkádová ovládací deska pro 2 hráče
- Playstation 4
- XBOX One

### Učebna - SW

Na každé pracovné stanici je nainstalována totožná konfigurace SW:

#### Development

- Visual Studio 2022
- Visual Studio Code
- Unity 2022
- Inkle

#### Grafika

- Blender
- Affinity Photo
- Affinity Designer

#### Zvuk

- Reaper

### Učebna - Sdílené úložiště

Pro zálohu dat a sdílení dat mezi jednotlivými PC je přístupné sdílené síťové úložiště, které naleznete v PC jako síťový disk.

## Učebna - Práce na PC

Pro bežné použití PC je vyhrazen volně přístupný účet **user**. Své soubory prosím ukládejte do vlastní složky pojmenované **Jmeno_Prijmeni**, kterou si založíte ve složce **Dokumenty**.

Studenti KHD mají možnost založit si na svém "kmenovém" PC vlastní účet pro ukládání citlivých dat (SSH klíče a podobně). Na tomto účtě však není zaručena funkčnost veškerého dostupného softvéru (zejména softvéru, který je licencován na konkrétní Windows účet).

## Rezervace učebny a instalace SW

- Mimo rozvrh a rezervované akce je učebna volně k dispozici pro práci na studentských projektech
- Učebna je k dispozici pro výuku i organizaci modulů ostatním katedrám FAMU. V případě zájmu prosím napište na <andrej.sykora@famu.cz>
- Učebna je k dispozici také všem studentům FAMU pro práci na vlastních studentských projektech
- Pro instalaci dalšího softvéru, resp. jiné akce vyžadujíci administrátorský přístup, prosím napište na <andrej.sykora@famu.cz>

## Další vybavení

Další vybavení je k dispozici pro zapujčení studentstvu KHD, resp. práci v učebně.

- 3x grafický tablet Huion Kamvas Pro 20 
- 4x grafický tablet Wacom Intuos M
- smartphone Samsung S20
- 2x iPad Pro se stylusem
- Nintendo Switch

Pro zapujčení / rezervaci vybavení prosím napište Andrejovi Sýkorovi na Discordu.

