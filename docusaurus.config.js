const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

// With JSDoc @type annotations, IDEs can provide config autocompletion
/** @type {import('@docusaurus/types').DocusaurusConfig} */
(module.exports = {
  title: 'Herní design | Manuál',
  tagline: '',
  url: 'https://hernidesign.famu.cz ',
  baseUrl: '/manual/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'manual-hernidesign-famu-cz', // Usually your repo name.

  presets: [
    [
      '@docusaurus/preset-classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl: 'https://github.com/facebook/docusaurus/edit/main/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/main/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Manuál',
        logo: {
          alt: 'Katedra herního designu FAMU',
          src: 'img/logo.svg',
        },
        items: [
          {
            to: 'docs/intro',
            position: 'left',
            label: 'Obecné návody',
          },
          {
            to: 'docs/courses/list',
            activeBasePath: 'docs/courses',
            position: 'left',
            label: 'Předměty',
          },
          {
            to: 'docs/materials/uvod-do-herniho-designu',
            activeBasePath: 'docs/materials',
            position: 'left',
            label: 'Studijní opory',
          },
          // {to: '/blog', label: 'Update Log', position: 'left'},
          {
            href: 'https://github.com/facebook/docusaurus',
            label: 'Zdrojáky',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Komunita',
            items: [
              {
                label: 'Discord',
                href: 'https://discord.gg/EZgc6ez6c3'
              },
              {
                label: 'Facebook',
                href: 'https://www.facebook.com/FamuHerniDesign/',
              },
              {
                label: 'Twitter',
                href: 'https://twitter.com/hernidesign',
              },
            ],
          },
          {
            title: 'Studium',
            items: [
              {
                label: 'KOS - Informační systém',
                href: 'https://kos.amu.cz',
              },
              {
                label: 'Studijní plán',
                href: 'https://sp.amu.cz/cs/stplan3033.html',
              },
            ],
          },
          {
            title: 'FAMU',
            items: [
              {
                label: 'Web Herního designu',
                href: 'https://hernidesign.famu.cz',
              },
              {
                label: 'Web FAMU',
                href: 'https://www.famu.cz',
              },
            ],
          },
        ]
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
});
